<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */


/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */


function choice_menu_ux_builder_element()
{
    add_ux_builder_shortcode('choice_menu', array(
        'name' => __('Choice Menu'),
        'category' => __('Content'),
        'priority' => 1,
        'options' => array(
            'text' => array(
                'type'       => 'textfield',
                'heading'    => 'Title',
                'default'    => 'Your Title',
                'auto_focus' => true,
            ),
            'ids' => array(
                'type' => 'select',
                'heading' => 'Chọn Menu',
                'param_name' => 'ids',
                'config' => array(
                    'multiple' => false,
                    'placeholder' => 'Select..',
                    'termSelect' => array(
                        'post_type' => 'nav_menu',
                        'taxonomies' => 'nav_menu'
                    ),
                )
            ),
        ),
    ));
}

add_action('ux_builder_setup', 'choice_menu_ux_builder_element');

function choice_menu_shortcode($atts)
{
    extract(shortcode_atts(array(
        'text' => 'Your Title',
        'ids' => false, // Custom IDs
    ), $atts));

//    echo "<pre>";
//    print_r(get_terms( 'nav_menu', array( 'hide_empty' => true ) ));
//    echo "</pre>";
    ?>
    <?php
    if ($ids == false) {
        return;
    }
//	$menu_name = 'main_nav';
//	$locations = get_nav_menu_locations();
//	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
//	$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

    $menuitems = wp_get_nav_menu_items( $ids, array( 'order' => 'DESC' ) );
    $menuitems = buildTree($menuitems);
    ob_start();
    ?>
    <h4><span style="font-size: 95%;"><strong><?php echo $text; ?></strong></span></h4>
    <ul class="menu border-none">
        <?php
        foreach( $menuitems as $item ){
            create_menu($item);
        }
        ?>
    </ul>
    <?php
    return ob_get_clean();
}

add_shortcode('choice_menu', 'choice_menu_shortcode');

function create_menu($item) {
    $link = $item->url;
    $title = $item->title;
    $id = $item->ID;
    if(property_exists($item, 'child')) {
        $children = $item->child;
        ?>
        <li class="menu-item menu-item-<?php echo $id; ?> menu-has-children">
            <a href="<?php echo $link; ?>">
                <?php echo $title; ?>
            </a>
            <ul class="sub-menu">
                <?php
                foreach($children as $child){
                    create_menu($child);
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="menu-item menu-item-<?php echo $id; ?>">
            <a href="<?php echo $link; ?>">
                <?php echo $title; ?>
            </a>
        </li>
        <?php
    }
}

function buildTree( array &$elements, $parentId = 0 )
{
    $branch = array();
    foreach ( $elements as &$element )
    {
        if ( $element->menu_item_parent == $parentId )
        {
            $children = buildTree( $elements, $element->ID );
            if ( $children )
                $element->child = $children;
            $element->has_children = 1;
            $branch[$element->ID] = $element;
            unset( $element );
        }
    }
    return $branch;
}


add_action( 'woocommerce_after_cart', function() {
   ?>
      <script>
         jQuery(function($) {
            var timeout;
            $('div.woocommerce').on('change textInput input', 'form.woocommerce-cart-form input.qty', function(){
               if(typeof timeout !== undefined) clearTimeout(timeout);
  
               //Not if empty
               if ($(this).val() == '') return;
  
               timeout = setTimeout(function() {
                  $("[name='update_cart']").trigger("click"); 
               }, 2000); // 2 second delay
            }); 
         });
      </script>
   <?php
} );

add_action( 'woocommerce_after_add_to_cart_button', 'misha_after_add_to_cart_btn' );
 
function misha_after_add_to_cart_btn(){
	?>
	<a class="buysll" href="tel:0388287614"> 
		<p>Mua số lượng lớn</p>
		<span>Gọi ngay 0388287614</span>
	</a>
   <?php
}





// remove_action('woocommerce_product_thumbnails','woocommerce_show_product_thumbnails',20);


// add_action('woocommerce_single_product_summary','woocommerce_show_product_thumbnails_start',15);
// add_action('woocommerce_single_product_summary','woocommerce_show_product_thumbnails',16);
// add_action('woocommerce_single_product_summary','woocommerce_show_product_thumbnails_end',17);

// function woocommerce_show_product_thumbnails_start() {
// echo '<div class="bbloomer-thumbs">';
// }
 
// function woocommerce_show_product_thumbnails_end() {
// echo '</div>';
// }





add_action('wp_enqueue_scripts', 'css_style');
function css_style(){
/* các flie css */
wp_register_style( 'styles-app', get_stylesheet_directory_uri() . '/css/main.css' );
wp_enqueue_style( 'styles-app' );
wp_register_style( 'styles-font-icon', 'https://use.fontawesome.com/releases/v5.0.1/css/all.css' );
wp_enqueue_style( 'styles-font-icon' );
}
// Tắt trình soạn thảo mới của WP trở về trình soạn thảo cũ
add_filter('use_block_editor_for_post', '__return_false');
