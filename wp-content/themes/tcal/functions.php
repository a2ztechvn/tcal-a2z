<?php
// Add custom Theme Functions here

add_action('woocommerce_before_single_product', 'move_variations_single_price', 1);
function move_variations_single_price()
{
    global $product, $post;
    if ($product->is_type('variable')) {
        add_action('woocommerce_single_product_summary', 'replace_variation_single_price', 10);
    }
}

function replace_variation_single_price()
{
    ?>
    <style>
        .woocommerce-variation-price {
            display: none;
        }
    </style>
    <script>
        jQuery(document).ready(function ($) {
            var priceselector = '.product p.price';
            var originalprice = $(priceselector).html();

            $(document).on('show_variation', function () {
                $(priceselector).html($('.single_variation .woocommerce-variation-price').html());
            });
            $(document).on('hide_variation', function () {
                $(priceselector).html(originalprice);
            });

            $('.accordion-title').click(function(){
                setTimeout(function(){
                    var sidebar = new StickySidebar('.is-sticky-column', {
                        topSpacing: 115,
                        bottomSpacing: 50,
                        containerSelector: '.content-row',
                        innerWrapperSelector: '.is-sticky-column__inner'
                    });
                }, 500);
            })
        });
    </script>
    <?php
}